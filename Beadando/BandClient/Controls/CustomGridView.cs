﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BandClient.Controls
{
    [Designer(typeof(System.Windows.Forms.Design.ControlDesigner))]
    public partial class CustomGridView : DataGridView
    {
        public CustomGridView() : base()
        {
            InitializeComponent();
        }
    }
}
