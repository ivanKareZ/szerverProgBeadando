﻿using BandClient.BandService;
using BandClient.GenreService;
using BandClient.Forms.FormBases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BandClient.Forms.RecordHandlerForms
{
    public partial class BandRecordHandlerForm : RecordEditFormBase
    {
        Band band;
        public Band Band
        {
            get { return band; }
        }

        Genre[] genres;

        public BandRecordHandlerForm(Band band) : base()
        {
            InitializeComponent();
            this.Text = "Zenekar szerkesztése";
            this.band = band;
            LoadBand();
            FillGenreOptions();
        }

        public BandRecordHandlerForm() : base()
        {
            InitializeComponent();
            this.Text = "Új zenekar hozzáadása";
            this.band = new Band();
            FillGenreOptions();
        }

        void FillGenreOptions()
        {
            try
            {
                GenreServiceClient genreService = new GenreServiceClient();
                genres = genreService.GetAllGenres(Session.visitorId);
                genreService.Close();
                genreBox.Items.Clear();
                foreach (Genre genre in genres)
                    genreBox.Items.Add(genre.name);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Hiba!");
            }
        }

        void LoadBand()
        {
            bandNameBox.Text = band.bandName;
            formationDateBox.Text = band.formationDate.ToString("yyyy-MM-dd");
            genreBox.Text = band.genre;
            descriptionBox.Text = band.description;
        }

        void Save()
        {
            band.bandName = bandNameBox.Text;
            band.description = descriptionBox.Text;
            band.genre = genreBox.Text;
            band.formationDate = DateTime.Parse(formationDateBox.Text);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
        }

        private void TextboxTextChange(object sender, EventArgs e)
        {
            saveButton.Enabled = IsTextboxesValid();
        }

        bool IsTextboxesValid()
        {
            DateTime t;
            return bandNameBox.Text != "" &&
                descriptionBox.Text != "" &&
                genreBox.Text != "" &&
                DateTime.TryParse(formationDateBox.Text, out t);
        }

        bool SaveGenre()
        {
            bool isExistingGenre = false;
            for (int i = 0; i < genres.Length; i++)
                if(genres[i].name == genreBox.Text)
                {
                    isExistingGenre = true;
                    break;
                }

            if (!isExistingGenre)
            {
                if (MessageBox.Show("A(z) "+genreBox.Text+" nevű műfaj még nem létezik! Létre akarod hozni most?", "Információ", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    GenreRecordHandlerForm genreHandler = new GenreRecordHandlerForm(genreBox.Text);
                    if (genreHandler.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        GenreServiceClient client = new GenreServiceClient();
                        client.AddGenre(Session.visitorId, genreHandler.Genre);
                        return true;
                    }
                    else return false;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private void BandRecordHandlerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                e.Cancel = !SaveGenre();
                Save();
            }
        }

    }
}
