﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BandClient.Forms.FormBases;
using BandClient.UserService;

namespace BandClient.Forms.RecordHandlerForms
{
    public partial class UserRecordHandler : RecordEditFormBase
    {
        User user;
        public User User
        {
            get { return user; }
        }

        bool isExistingUser;

        public UserRecordHandler(User user) : base()
        {
            this.user = user;
            InitializeComponent();
            isExistingUser = user != null;
            if (!isExistingUser) FillEmpty();
            else FillFromUser();
        }

        void FillFromUser()
        {
            this.Text = "Felhasználó szerkesztése";
            fullNameBox.Text = user.fullName;
            usernameBox.Text = user.userName;
            passwordBox.Text = user.passWord;
            regDateTextbox.Text = user.regDate.ToString();
            adminCheckBox.Checked = user.isAdmin;
        }

        void FillEmpty()
        {
            this.Text = "Felhasználó hozzáadása";
        }

        void SaveUser()
        {
            user.passWord = passwordBox.Text;
            user.fullName = fullNameBox.Text;
            user.userName = usernameBox.Text;
            user.isAdmin = adminCheckBox.Checked;
        }

        bool IsInputsValid()
        {
            return passwordBox.Text != "" && fullNameBox.Text != "" && usernameBox.Text != "";
        }

        private void TextboxTextChange(object sender, EventArgs e)
        {
            saveButton.Enabled = IsInputsValid();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (!isExistingUser)
                user = new User();
            SaveUser();
        }
    }
}
