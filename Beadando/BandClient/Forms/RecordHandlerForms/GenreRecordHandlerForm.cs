﻿using BandClient.Forms.FormBases;
using BandClient.GenreService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BandClient.Forms.RecordHandlerForms
{
    public partial class GenreRecordHandlerForm : RecordEditFormBase
    {
        Genre genre;
        public Genre Genre
        {
            get { return genre; }
        }

        public GenreRecordHandlerForm() : base()
        {
            InitializeComponent();
            this.genre = new Genre();
            LoadGenre();
        }

        public GenreRecordHandlerForm(string newGenre) : base()
        {
            InitializeComponent();
            this.genre = new Genre();
            this.genre.name = newGenre;
            LoadGenre();
        }

        public GenreRecordHandlerForm(Genre genre) : base()
        {
            InitializeComponent();
            this.genre = genre;
            LoadGenre();
        }

        void LoadGenre()
        {
            descriptionBox.Text = genre.description;
            nameBox.Text = genre.name;
        }

        private void TextboxTextchange(object sender, EventArgs e)
        {
            saveButton.Enabled = descriptionBox.Text != "" && nameBox.Text != "";
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            genre.name = nameBox.Text;
            genre.description = descriptionBox.Text;
        }
    }
}
