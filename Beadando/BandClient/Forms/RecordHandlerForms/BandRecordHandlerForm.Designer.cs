﻿namespace BandClient.Forms.RecordHandlerForms
{
    partial class BandRecordHandlerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bandNameBox = new System.Windows.Forms.TextBox();
            this.formationDateBox = new System.Windows.Forms.TextBox();
            this.descriptionBox = new System.Windows.Forms.TextBox();
            this.genreBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // saveButton
            // 
            this.saveButton.Enabled = false;
            this.saveButton.Location = new System.Drawing.Point(241, 194);
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(12, 194);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Zenekar neve:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Alakulás ideje:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Műfaj:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Leírás:";
            // 
            // bandNameBox
            // 
            this.bandNameBox.Location = new System.Drawing.Point(95, 13);
            this.bandNameBox.Name = "bandNameBox";
            this.bandNameBox.Size = new System.Drawing.Size(221, 20);
            this.bandNameBox.TabIndex = 6;
            this.bandNameBox.TextChanged += new System.EventHandler(this.TextboxTextChange);
            // 
            // formationDateBox
            // 
            this.formationDateBox.Location = new System.Drawing.Point(95, 39);
            this.formationDateBox.Name = "formationDateBox";
            this.formationDateBox.Size = new System.Drawing.Size(221, 20);
            this.formationDateBox.TabIndex = 7;
            this.formationDateBox.TextChanged += new System.EventHandler(this.TextboxTextChange);
            // 
            // descriptionBox
            // 
            this.descriptionBox.Location = new System.Drawing.Point(12, 114);
            this.descriptionBox.Multiline = true;
            this.descriptionBox.Name = "descriptionBox";
            this.descriptionBox.Size = new System.Drawing.Size(304, 74);
            this.descriptionBox.TabIndex = 9;
            this.descriptionBox.TextChanged += new System.EventHandler(this.TextboxTextChange);
            // 
            // genreBox
            // 
            this.genreBox.FormattingEnabled = true;
            this.genreBox.Location = new System.Drawing.Point(95, 65);
            this.genreBox.Name = "genreBox";
            this.genreBox.Size = new System.Drawing.Size(221, 21);
            this.genreBox.TabIndex = 10;
            this.genreBox.TextChanged += new System.EventHandler(this.TextboxTextChange);
            // 
            // BandRecordHandlerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 224);
            this.Controls.Add(this.genreBox);
            this.Controls.Add(this.descriptionBox);
            this.Controls.Add(this.formationDateBox);
            this.Controls.Add(this.bandNameBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "BandRecordHandlerForm";
            this.Text = "Zenekar";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BandRecordHandlerForm_FormClosing);
            this.Controls.SetChildIndex(this.saveButton, 0);
            this.Controls.SetChildIndex(this.cancelButton, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.bandNameBox, 0);
            this.Controls.SetChildIndex(this.formationDateBox, 0);
            this.Controls.SetChildIndex(this.descriptionBox, 0);
            this.Controls.SetChildIndex(this.genreBox, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox bandNameBox;
        private System.Windows.Forms.TextBox formationDateBox;
        private System.Windows.Forms.TextBox descriptionBox;
        private System.Windows.Forms.ComboBox genreBox;
    }
}