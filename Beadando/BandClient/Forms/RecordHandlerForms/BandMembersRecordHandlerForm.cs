﻿using BandClient.UserService;
using BandClient.Forms.FormBases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BandClient.Forms.RecordHandlerForms
{
    public partial class BandMembersRecordHandlerForm : RecordEditFormBase
    {
        public int SelectedUserId { get; protected set; }
        User[] users;
        public BandMembersRecordHandlerForm(string bandName, User[] users) 
        {
            InitializeComponent();
            this.Text = "Új tag hozzáadása (" + bandName + ")";
            this.users = users;
            FillList("");
        }

        void FillList(string filter)
        {
            userListBox.Items.Clear();
            foreach (User item in users)
            {
                if (item.fullName.Contains(filter) || item.userName.Contains(filter))
                {
                    userListBox.Items.Add(String.Format("{0} ({1})", item.fullName, item.userName));
                }
            }
        }

        private void filterTextBoxTextChanged(object sender, EventArgs e)
        {
            FillList(filterTextBox.Text);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (users.Length > 0)
            {
                int index = userListBox.SelectedIndex;
                SelectedUserId = users[index].id;
            }
            else
            {
                SelectedUserId = -1;
            }
        }
    }
}
