﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BandClient;
using BandClient.UserService;
using System.ServiceModel;

namespace BandClient.Forms
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            userNameBox.Text = BandClient.Properties.Settings.Default.LastUserName;
            if (userNameBox.Text != "") 
                this.ActiveControl = passwordBox;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (userNameBox.Text == "" || passwordBox.Text == "")
                MessageBox.Show("Kérem minden mezőt töltsön ki", "Hiba!");
            else
            {
                try
                {
                    UserServiceClient client = new UserServiceClient();
                    int myId = client.LoginUser(userNameBox.Text, passwordBox.Text);
                    client.Close();
                    Session.StartSession(myId, userNameBox.Text);
                    BandClient.Properties.Settings.Default.LastUserName = userNameBox.Text;
                    BandClient.Properties.Settings.Default.Save();
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                }
                catch (FaultException fault)
                {
                    MessageBox.Show(fault.Reason.ToString(), "Hiba!");
                    userNameBox.Focus();
                    userNameBox.Select(0, userNameBox.Text.Length);
                }
                catch
                {
                    MessageBox.Show("Ismeretlen hiba történt. Kérem próbálja újra!", "Hiba!");
                    userNameBox.Focus();
                    userNameBox.Select(0, userNameBox.Text.Length);
                }
            }
        }
    }
}
