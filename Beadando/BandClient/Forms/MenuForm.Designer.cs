﻿namespace BandClient.Forms
{
    partial class MenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sessionTimeStartLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bandMemberHandlerButton = new System.Windows.Forms.Button();
            this.genreHandlerButton = new System.Windows.Forms.Button();
            this.bandHandlerButton = new System.Windows.Forms.Button();
            this.userHandlerButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.sessionTimeStartLabel);
            this.groupBox1.Controls.Add(this.usernameLabel);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(466, 51);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Munkamenet adatai";
            // 
            // sessionTimeStartLabel
            // 
            this.sessionTimeStartLabel.AutoSize = true;
            this.sessionTimeStartLabel.Location = new System.Drawing.Point(205, 26);
            this.sessionTimeStartLabel.Name = "sessionTimeStartLabel";
            this.sessionTimeStartLabel.Size = new System.Drawing.Size(113, 13);
            this.sessionTimeStartLabel.TabIndex = 1;
            this.sessionTimeStartLabel.Text = "Munkamenet kezdete:";
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Location = new System.Drawing.Point(6, 26);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(84, 13);
            this.usernameLabel.TabIndex = 0;
            this.usernameLabel.Text = "Felhasználónév:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bandMemberHandlerButton);
            this.groupBox2.Controls.Add(this.genreHandlerButton);
            this.groupBox2.Controls.Add(this.bandHandlerButton);
            this.groupBox2.Controls.Add(this.userHandlerButton);
            this.groupBox2.Location = new System.Drawing.Point(13, 70);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(466, 62);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Adatbázis kezelése";
            // 
            // bandMemberHandlerButton
            // 
            this.bandMemberHandlerButton.Location = new System.Drawing.Point(279, 26);
            this.bandMemberHandlerButton.Name = "bandMemberHandlerButton";
            this.bandMemberHandlerButton.Size = new System.Drawing.Size(168, 23);
            this.bandMemberHandlerButton.TabIndex = 5;
            this.bandMemberHandlerButton.Text = "Zenekarok tagjai";
            this.bandMemberHandlerButton.UseVisualStyleBackColor = true;
            this.bandMemberHandlerButton.Click += new System.EventHandler(this.bandMemberHandlerButton_Click);
            // 
            // genreHandlerButton
            // 
            this.genreHandlerButton.Location = new System.Drawing.Point(192, 26);
            this.genreHandlerButton.Name = "genreHandlerButton";
            this.genreHandlerButton.Size = new System.Drawing.Size(81, 23);
            this.genreHandlerButton.TabIndex = 2;
            this.genreHandlerButton.Text = "Műfajok";
            this.genreHandlerButton.UseVisualStyleBackColor = true;
            this.genreHandlerButton.Click += new System.EventHandler(this.genreHandlerButton_Click);
            // 
            // bandHandlerButton
            // 
            this.bandHandlerButton.Location = new System.Drawing.Point(105, 26);
            this.bandHandlerButton.Name = "bandHandlerButton";
            this.bandHandlerButton.Size = new System.Drawing.Size(81, 23);
            this.bandHandlerButton.TabIndex = 1;
            this.bandHandlerButton.Text = "Zenekarok";
            this.bandHandlerButton.UseVisualStyleBackColor = true;
            this.bandHandlerButton.Click += new System.EventHandler(this.bandHandlerButton_Click);
            // 
            // userHandlerButton
            // 
            this.userHandlerButton.Location = new System.Drawing.Point(18, 26);
            this.userHandlerButton.Name = "userHandlerButton";
            this.userHandlerButton.Size = new System.Drawing.Size(81, 23);
            this.userHandlerButton.TabIndex = 0;
            this.userHandlerButton.Text = "Felhasználók";
            this.userHandlerButton.UseVisualStyleBackColor = true;
            this.userHandlerButton.Click += new System.EventHandler(this.userHandlerButton_Click);
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 140);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MenuForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menü";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label sessionTimeStartLabel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button userHandlerButton;
        private System.Windows.Forms.Button bandHandlerButton;
        private System.Windows.Forms.Button bandMemberHandlerButton;
        private System.Windows.Forms.Button genreHandlerButton;
    }
}