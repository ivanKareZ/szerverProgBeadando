﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BandClient.Forms.TableHandlerForms;

namespace BandClient.Forms
{
    public partial class MenuForm : Form
    {
        public MenuForm()
        {
            InitializeComponent();
            usernameLabel.Text = "Felhasználónév: " + Session.userName;
            sessionTimeStartLabel.Text = "Munkamenet kezdete: " + Session.sessionStartTime;
        }

        private void userHandlerButton_Click(object sender, EventArgs e)
        {
            UserTableHandlerForm userForm = new UserTableHandlerForm();
            userForm.ShowDialog();
        }

        private void bandHandlerButton_Click(object sender, EventArgs e)
        {
            BandTableHandlerForm bandForm = new BandTableHandlerForm();
            bandForm.ShowDialog();
        }

        private void genreHandlerButton_Click(object sender, EventArgs e)
        {
            GenreTableHandlerForm genreForm = new GenreTableHandlerForm();
            genreForm.ShowDialog();
        }

        private void bandMemberHandlerButton_Click(object sender, EventArgs e)
        {
            BandMemberHandlerForm memberForm = new BandMemberHandlerForm();
            memberForm.ShowDialog();
        }
    }
}
