﻿namespace BandClient.Forms.TableHandlerForms
{
    partial class BandMemberHandlerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bandGroupBox = new System.Windows.Forms.GroupBox();
            this.bandFilterButton = new System.Windows.Forms.Button();
            this.bandFilterTextbox = new System.Windows.Forms.TextBox();
            this.bandRefreshButton = new System.Windows.Forms.Button();
            this.memberGoupBox = new System.Windows.Forms.GroupBox();
            this.refreshButton = new System.Windows.Forms.Button();
            this.deleteAllMemberButton = new System.Windows.Forms.Button();
            this.deleteMemberButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.memberGridView = new BandClient.Controls.CustomGridView();
            this.bandGridView = new BandClient.Controls.CustomGridView();
            this.bandGroupBox.SuspendLayout();
            this.memberGoupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memberGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // bandGroupBox
            // 
            this.bandGroupBox.Controls.Add(this.bandFilterButton);
            this.bandGroupBox.Controls.Add(this.bandFilterTextbox);
            this.bandGroupBox.Controls.Add(this.bandRefreshButton);
            this.bandGroupBox.Controls.Add(this.bandGridView);
            this.bandGroupBox.Location = new System.Drawing.Point(13, 13);
            this.bandGroupBox.Name = "bandGroupBox";
            this.bandGroupBox.Size = new System.Drawing.Size(688, 274);
            this.bandGroupBox.TabIndex = 0;
            this.bandGroupBox.TabStop = false;
            this.bandGroupBox.Text = "Zenekarok";
            // 
            // bandFilterButton
            // 
            this.bandFilterButton.Location = new System.Drawing.Point(607, 221);
            this.bandFilterButton.Name = "bandFilterButton";
            this.bandFilterButton.Size = new System.Drawing.Size(75, 23);
            this.bandFilterButton.TabIndex = 8;
            this.bandFilterButton.Text = "Szűrés";
            this.bandFilterButton.UseVisualStyleBackColor = true;
            this.bandFilterButton.Click += new System.EventHandler(this.bandFilterButton_Click);
            // 
            // bandFilterTextbox
            // 
            this.bandFilterTextbox.Location = new System.Drawing.Point(364, 223);
            this.bandFilterTextbox.Name = "bandFilterTextbox";
            this.bandFilterTextbox.Size = new System.Drawing.Size(237, 20);
            this.bandFilterTextbox.TabIndex = 7;
            // 
            // bandRefreshButton
            // 
            this.bandRefreshButton.Location = new System.Drawing.Point(6, 221);
            this.bandRefreshButton.Name = "bandRefreshButton";
            this.bandRefreshButton.Size = new System.Drawing.Size(108, 22);
            this.bandRefreshButton.TabIndex = 6;
            this.bandRefreshButton.Text = "Frissítés";
            this.bandRefreshButton.UseVisualStyleBackColor = true;
            this.bandRefreshButton.Click += new System.EventHandler(this.bandRefreshButton_Click);
            // 
            // memberGoupBox
            // 
            this.memberGoupBox.Controls.Add(this.refreshButton);
            this.memberGoupBox.Controls.Add(this.deleteAllMemberButton);
            this.memberGoupBox.Controls.Add(this.deleteMemberButton);
            this.memberGoupBox.Controls.Add(this.addButton);
            this.memberGoupBox.Controls.Add(this.memberGridView);
            this.memberGoupBox.Location = new System.Drawing.Point(12, 293);
            this.memberGoupBox.Name = "memberGoupBox";
            this.memberGoupBox.Size = new System.Drawing.Size(688, 291);
            this.memberGoupBox.TabIndex = 1;
            this.memberGoupBox.TabStop = false;
            this.memberGoupBox.Text = "Tagok";
            // 
            // refreshButton
            // 
            this.refreshButton.Location = new System.Drawing.Point(120, 249);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(108, 22);
            this.refreshButton.TabIndex = 5;
            this.refreshButton.Text = "Frissítés";
            this.refreshButton.UseVisualStyleBackColor = true;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // deleteAllMemberButton
            // 
            this.deleteAllMemberButton.Location = new System.Drawing.Point(120, 221);
            this.deleteAllMemberButton.Name = "deleteAllMemberButton";
            this.deleteAllMemberButton.Size = new System.Drawing.Size(108, 22);
            this.deleteAllMemberButton.TabIndex = 4;
            this.deleteAllMemberButton.Text = "Összes tag törlése";
            this.deleteAllMemberButton.UseVisualStyleBackColor = true;
            this.deleteAllMemberButton.Click += new System.EventHandler(this.deleteAllMemberButton_Click);
            // 
            // deleteMemberButton
            // 
            this.deleteMemberButton.Location = new System.Drawing.Point(6, 250);
            this.deleteMemberButton.Name = "deleteMemberButton";
            this.deleteMemberButton.Size = new System.Drawing.Size(108, 22);
            this.deleteMemberButton.TabIndex = 3;
            this.deleteMemberButton.Text = "Tag törlése";
            this.deleteMemberButton.UseVisualStyleBackColor = true;
            this.deleteMemberButton.Click += new System.EventHandler(this.deleteMemberButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(6, 221);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(108, 23);
            this.addButton.TabIndex = 2;
            this.addButton.Text = "Tag hozzáadása";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // memberGridView
            // 
            this.memberGridView.AllowUserToAddRows = false;
            this.memberGridView.AllowUserToDeleteRows = false;
            this.memberGridView.Location = new System.Drawing.Point(6, 19);
            this.memberGridView.MultiSelect = false;
            this.memberGridView.Name = "memberGridView";
            this.memberGridView.ReadOnly = true;
            this.memberGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.memberGridView.ShowEditingIcon = false;
            this.memberGridView.Size = new System.Drawing.Size(676, 196);
            this.memberGridView.TabIndex = 1;
            // 
            // bandGridView
            // 
            this.bandGridView.AllowUserToAddRows = false;
            this.bandGridView.AllowUserToDeleteRows = false;
            this.bandGridView.Location = new System.Drawing.Point(6, 19);
            this.bandGridView.MultiSelect = false;
            this.bandGridView.Name = "bandGridView";
            this.bandGridView.ReadOnly = true;
            this.bandGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.bandGridView.ShowEditingIcon = false;
            this.bandGridView.Size = new System.Drawing.Size(676, 196);
            this.bandGridView.TabIndex = 0;
            this.bandGridView.SelectionChanged += new System.EventHandler(this.bandGridView_SelectionChanged);
            // 
            // BandMemberHandlerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(707, 596);
            this.Controls.Add(this.memberGoupBox);
            this.Controls.Add(this.bandGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BandMemberHandlerForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Zenekarok tagjai";
            this.bandGroupBox.ResumeLayout(false);
            this.bandGroupBox.PerformLayout();
            this.memberGoupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memberGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox bandGroupBox;
        private System.Windows.Forms.GroupBox memberGoupBox;
        private Controls.CustomGridView bandGridView;
        private Controls.CustomGridView memberGridView;
        private System.Windows.Forms.Button deleteAllMemberButton;
        private System.Windows.Forms.Button deleteMemberButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.Button bandRefreshButton;
        private System.Windows.Forms.Button bandFilterButton;
        private System.Windows.Forms.TextBox bandFilterTextbox;
    }
}