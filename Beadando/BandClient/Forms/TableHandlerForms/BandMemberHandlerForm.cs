﻿using BandClient.BandService;
using BandClient.UserService;
using BandClient.BandMemberService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BandClient.Forms.RecordHandlerForms;

namespace BandClient.Forms.TableHandlerForms
{
    public partial class BandMemberHandlerForm : Form
    {
        string bandHeaderText = "";
        string memberHeaderText = "";
        public BandMemberHandlerForm()
        {
            InitializeComponent();
            bandHeaderText = bandGroupBox.Text;
            memberHeaderText = memberGoupBox.Text;
            GetAllBands();
            GetAllUsers();
        }

        void GetAllBands()
        {
            try
            {
                bandGroupBox.Text = bandHeaderText;
                BandServiceClient client = new BandServiceClient();
                Band[] bands = client.GetAllBand(Session.visitorId);
                client.Close();
                RefreshBandGridView(bands);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }

        void GetFilteredBands(string filter)
        {
            try
            {
                bandGroupBox.Text = bandHeaderText + " (szűrt)";
                BandServiceClient client = new BandServiceClient();
                Band[] bands = client.GetBandsFiltered(Session.visitorId,filter);
                client.Close();
                RefreshBandGridView(bands);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }

        private void RefreshBandGridView(Band[] bands)
        {
            bandGridView.Columns.Clear();
            bandGridView.Columns.Add("id", "Azonosító");
            bandGridView.Columns.Add("bandname", "Zenekar neve");
            bandGridView.Columns.Add("formationDate", "Alakulás ideje");
            bandGridView.Columns.Add("description", "Leírás");
            bandGridView.Columns["description"].Width = 300;
            bandGridView.Columns.Add("genre", "Műfaj");

            bandGridView.Columns["id"].Visible = false;

            for (int i = 0; i < bands.Length; i++)
            {
                Band band = bands[i];
                bandGridView.Rows.Add(band.id, band.bandName, band.formationDate.ToString("yyyy-MM-dd"), band.description, band.genre);
            }
        }

        void GetAllUsers()
        {
            if (bandGridView.SelectedRows.Count > 0)
            {
                int id = GetSelectedBand().id;
                memberGoupBox.Text = memberHeaderText;
                BandMemberService.BandMembersServiceClient client = new BandMemberService.BandMembersServiceClient();
                BandMemberService.User[] users = client.GetUsersFromBand(Session.visitorId, id);
                client.Close();
                if (users == null)
                    return;
                RefreshGridView(users);
            }
        }

        void RefreshGridView(BandMemberService.User[] users)
        {
            memberGridView.Columns.Clear();
            memberGridView.Columns.Add("id", "Azonosító");
            memberGridView.Columns.Add("password", "Jelszó");
            memberGridView.Columns.Add("fullname", "Teljes név");
            memberGridView.Columns.Add("username", "Felhasználó név");
            memberGridView.Columns.Add("regDate", "Regisztráció ideje");
            int adminColumn = memberGridView.Columns.Add(new DataGridViewCheckBoxColumn());
            memberGridView.Columns[adminColumn].HeaderText = "Adminisztrátor";
            memberGridView.Columns[adminColumn].Name = "isAdmin";
            memberGridView.Columns["id"].Visible = false;
            memberGridView.Columns["password"].Visible = false;
            memberGridView.Columns["regDate"].Width = 200;

            for (int i = 0; i < users.Length; i++)
            {
                BandMemberService.User user = users[i];
                memberGridView.Rows.Add(user.id, user.passWord, user.fullName, user.userName, user.regDate, user.isAdmin);
            }
        }

        Band GetSelectedBand()
        {
            DataGridViewRow row = bandGridView.SelectedRows[0];

            Band band = new Band();

            band.id = int.Parse(row.Cells["id"].FormattedValue.ToString());
            band.bandName = row.Cells["bandname"].FormattedValue.ToString();
            band.formationDate = DateTime.Parse(row.Cells["formationDate"].FormattedValue.ToString());
            band.description = row.Cells["description"].FormattedValue.ToString();
            band.genre = row.Cells["genre"].FormattedValue.ToString();

            return band;
        }

        BandMemberService.User GetSelectedUser()
        {
            if (memberGridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("Nincs rekord kiválasztva!", "Hiba!");
                return null;
            }

            DataGridViewRow row = memberGridView.SelectedRows[0];

            BandMemberService.User user = new BandMemberService.User();

            user.id = int.Parse(row.Cells["id"].FormattedValue.ToString());
            user.passWord = row.Cells["password"].FormattedValue.ToString();
            user.fullName = row.Cells["fullname"].FormattedValue.ToString();
            user.userName = row.Cells["username"].FormattedValue.ToString();
            user.regDate = DateTime.Parse(row.Cells["regDate"].FormattedValue.ToString());
            user.isAdmin = bool.Parse(row.Cells["isAdmin"].FormattedValue.ToString());

            return user;
        }

        private void bandGridView_SelectionChanged(object sender, EventArgs e)
        {
            GetAllUsers();
        }

        private void bandFilterButton_Click(object sender, EventArgs e)
        {
            string filter = FilterStringBuilder.Replace(bandFilterTextbox.Text);
            GetFilteredBands(filter);
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            string selectedBand = GetSelectedBand().bandName;
            UserServiceClient uservice = new UserServiceClient();
            UserService.User[] users = uservice.GetAllUser(Session.visitorId);
            uservice.Close();
            BandMembersRecordHandlerForm form = new BandMembersRecordHandlerForm(selectedBand, users);
            if(form.ShowDialog() == DialogResult.OK && form.SelectedUserId != -1)
            {
                BandMembersServiceClient client = new BandMembersServiceClient();
                BandMembers m = new BandMembers();
                m.bandId = GetSelectedBand().id;
                m.userId = form.SelectedUserId;
                client.Insert(Session.visitorId, m);
                client.Close();
                GetAllUsers();
            }

        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            GetAllUsers();
        }

        private void bandRefreshButton_Click(object sender, EventArgs e)
        {
            GetAllBands();
        }

        private void deleteMemberButton_Click(object sender, EventArgs e)
        {
            BandMembersServiceClient client = new BandMembersServiceClient();
            BandMembers m = new BandMembers();
            m.bandId = GetSelectedBand().id;
            m.userId = GetSelectedUser().id;
            client.Delete(Session.visitorId, m.bandId, m.userId);
            client.Close();
            GetAllUsers();
        }

        private void deleteAllMemberButton_Click(object sender, EventArgs e)
        {
            BandMembersServiceClient client = new BandMembersServiceClient();
            BandMembers m = new BandMembers();
            m.bandId = GetSelectedBand().id;
            client.DeleteAll(Session.visitorId, m.bandId);
            client.Close();
            GetAllUsers();
        }
    }
}
