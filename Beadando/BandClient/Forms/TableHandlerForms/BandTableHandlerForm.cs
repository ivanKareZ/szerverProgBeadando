﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BandClient.Forms.FormBases;
using BandClient.BandService;
using BandClient.Forms.RecordHandlerForms;

namespace BandClient.Forms.TableHandlerForms
{
    public partial class BandTableHandlerForm : SingleTableHandlerForm
    {
        string headerText = "";
        public BandTableHandlerForm()
            : base()
        {
            InitializeComponent();
            headerText = this.Text;
            GetAllBands();
        }

        void GetAllBands()
        {
            try
            {
                this.Text = headerText;
                BandServiceClient client = new BandServiceClient();
                Band[] bands = client.GetAllBand(Session.visitorId);
                client.Close();
                RefreshGridView(bands);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }

        Band GetSelectedBandFromGrid()
        {
            if (gridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("Nincs rekord kiválasztva!", "Hiba!");
                return null;
            }

            DataGridViewRow row = gridView.SelectedRows[0];

            Band band = new Band();

            band.id = int.Parse(row.Cells["id"].FormattedValue.ToString());
            band.bandName = row.Cells["bandname"].FormattedValue.ToString();
            band.formationDate = DateTime.Parse(row.Cells["formationDate"].FormattedValue.ToString());
            band.description = row.Cells["description"].FormattedValue.ToString();
            band.genre = row.Cells["genre"].FormattedValue.ToString();

            return band;
        }

        void RefreshGridView(Band[] bands)
        {
            gridView.Columns.Clear();
            gridView.Columns.Add("id", "Azonosító");
            gridView.Columns.Add("bandname", "Zenekar neve");
            gridView.Columns.Add("formationDate", "Alakulás ideje");
            gridView.Columns.Add("description", "Leírás");
            gridView.Columns["description"].Width = 300;
            gridView.Columns.Add("genre", "Műfaj");

            gridView.Columns["id"].Visible = false;

            for (int i = 0; i < bands.Length; i++)
            {
                Band band = bands[i];
                gridView.Rows.Add(band.id, band.bandName, band.formationDate.ToString("yyyy-MM-dd"), band.description, band.genre);
            }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            GetAllBands();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            Band band = GetSelectedBandFromGrid();
            try
            {
                BandServiceClient client = new BandServiceClient();
                client.DeleteBand(Session.visitorId, band.id);
                client.Close();
                GetAllBands();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }

        private void deleteAllButton_Click(object sender, EventArgs e)
        {
            try
            {
                BandServiceClient client = new BandServiceClient();
                client.DeleteAllBands(Session.visitorId);
                client.Close();
                GetAllBands();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            Band band = GetSelectedBandFromGrid();

            BandRecordHandlerForm window = new BandRecordHandlerForm(band);
            window.ShowDialog();
            if (window.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    BandServiceClient client = new BandServiceClient();
                    client.UpdateBand(Session.visitorId, window.Band);
                    client.Close();
                    GetAllBands();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
                }
            }
        }

        private void filterButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.Text = headerText+" (Szűrt)";
                string filterText = FilterStringBuilder.Replace(filterTextBox.Text);
                BandServiceClient client = new BandServiceClient();
                Band[] bands = client.GetBandsFiltered(Session.visitorId, filterText);
                client.Close();
                RefreshGridView(bands);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            BandRecordHandlerForm window = new BandRecordHandlerForm();
            window.ShowDialog();
            if (window.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    BandServiceClient client = new BandServiceClient();
                    client.AddBand(Session.visitorId, window.Band);
                    client.Close();
                    GetAllBands();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
                }
            }
        }
    }
}
