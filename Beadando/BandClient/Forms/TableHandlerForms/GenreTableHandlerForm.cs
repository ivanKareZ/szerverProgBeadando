﻿using BandClient.Forms.FormBases;
using BandClient.Forms.RecordHandlerForms;
using BandClient.GenreService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BandClient.Forms.TableHandlerForms
{
    public partial class GenreTableHandlerForm : SingleTableHandlerForm
    {
        string headerText;
        public GenreTableHandlerForm()
        {
            InitializeComponent();
            headerText = this.Text;
            GetAllGenre();
        }

        void GetAllGenre()
        {
            try
            {
                this.Text = headerText;
                GenreServiceClient client = new GenreServiceClient();
                Genre[] genres = client.GetAllGenres(Session.visitorId);
                client.Close();
                RefreshGridView(genres);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }

        Genre GetSelectedGenreFromGrid()
        {
            if (gridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("Nincs rekord kiválasztva!", "Hiba!");
                return null;
            }

            DataGridViewRow row = gridView.SelectedRows[0];

            Genre genre = new Genre();

            genre.id = int.Parse(row.Cells["id"].FormattedValue.ToString());
            genre.name = row.Cells["name"].FormattedValue.ToString();
            genre.description = row.Cells["description"].FormattedValue.ToString();

            return genre;
        }

        private void RefreshGridView(Genre[] genres)
        {
            gridView.Columns.Clear();
            gridView.Columns.Add("id", "Azonosító");
            gridView.Columns.Add("name", "Műfaj neve");
            gridView.Columns.Add("description", "Leírás");
            gridView.Columns["description"].Width = 400;

            gridView.Columns["id"].Visible = false;

            for (int i = 0; i < genres.Length; i++)
            {
                Genre genre = genres[i];
                gridView.Rows.Add(genre.id, genre.name, genre.description);
            }
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            GenreRecordHandlerForm window = new GenreRecordHandlerForm();
            window.ShowDialog();
            if (window.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    GenreServiceClient client = new GenreServiceClient();
                    client.AddGenre(Session.visitorId, window.Genre);
                    client.Close();
                    GetAllGenre();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
                }
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            Genre selected = GetSelectedGenreFromGrid();
            GenreRecordHandlerForm window = new GenreRecordHandlerForm(selected);
            window.ShowDialog();
            if (window.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    GenreServiceClient client = new GenreServiceClient();
                    client.UpdateGenre(Session.visitorId, window.Genre);
                    client.Close();
                    GetAllGenre();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
                }
            }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            GetAllGenre();
        }

        private void deleteAllButton_Click(object sender, EventArgs e)
        {
            try
            {
                GenreServiceClient client = new GenreServiceClient();
                client.DeleteAllGenres(Session.visitorId);
                client.Close();
                GetAllGenre();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            Genre selected = GetSelectedGenreFromGrid();
            try
            {
                GenreServiceClient client = new GenreServiceClient();
                client.DeleteGenre(Session.visitorId,selected.id);
                client.Close();
                GetAllGenre();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }

        private void filterButton_Click(object sender, EventArgs e)
        {
            string filterText = FilterStringBuilder.Replace(filterTextBox.Text);
            try
            {
                this.Text = headerText;
                GenreServiceClient client = new GenreServiceClient();
                Genre[] genres = client.GetGenresFiltered(Session.visitorId,filterText);
                client.Close();
                this.Text = headerText + " (Szűrt)";
                RefreshGridView(genres);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }
    }
}
