﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BandClient.Forms.FormBases;
using BandClient.UserService;
using BandClient.Forms.RecordHandlerForms;
using System.Diagnostics;

namespace BandClient.Forms.TableHandlerForms
{
    public partial class UserTableHandlerForm : SingleTableHandlerForm
    {
        string simpleWindowText;

        public UserTableHandlerForm() : base()
        {
            InitializeComponent();
            simpleWindowText = this.Text;
            GetAllUsers();
        }

        void GetAllUsers()
        {
            this.Text = simpleWindowText;
            UserServiceClient client = new UserServiceClient();
            User[] users = client.GetAllUser(Session.visitorId);
            client.Close();
            if (users == null)
                return;
            RefreshGridView(users);
        }

        void RefreshGridView(User[] users)
        {
            gridView.Columns.Clear();
            gridView.Columns.Add("id", "Azonosító");
            gridView.Columns.Add("password", "Jelszó");
            gridView.Columns.Add("fullname", "Teljes név");
            gridView.Columns.Add("username", "Felhasználó név");
            gridView.Columns.Add("regDate", "Regisztráció ideje");
            int adminColumn = gridView.Columns.Add(new DataGridViewCheckBoxColumn());
            gridView.Columns[adminColumn].HeaderText = "Adminisztrátor";
            gridView.Columns[adminColumn].Name = "isAdmin";

            gridView.Columns["id"].Visible = false;
            gridView.Columns["password"].Visible = false;
            gridView.Columns["regDate"].Width = 200;

            for (int i = 0; i < users.Length; i++)
            {
                User user = users[i];
                gridView.Rows.Add(user.id, user.passWord, user.fullName, user.userName, user.regDate, user.isAdmin);
            }
        }

        User GetSelectedUserFromGrid()
        {
            if(gridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("Nincs rekord kiválasztva!", "Hiba!");
                return null;
            }

            DataGridViewRow row = gridView.SelectedRows[0];

            User user = new User();

            user.id = int.Parse(row.Cells["id"].FormattedValue.ToString());
            user.passWord = row.Cells["password"].FormattedValue.ToString();
            user.fullName = row.Cells["fullname"].FormattedValue.ToString();
            user.userName = row.Cells["username"].FormattedValue.ToString();
            user.regDate = DateTime.Parse(row.Cells["regDate"].FormattedValue.ToString());
            user.isAdmin = bool.Parse(row.Cells["isAdmin"].FormattedValue.ToString());

            return user;
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            User selected = GetSelectedUserFromGrid();
            if (selected == null) return;

            UserRecordHandler window = new UserRecordHandler(selected);
            window.ShowDialog();

            if (window.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    UserServiceClient client = new UserServiceClient();
                    client.UpdateUser(window.User, Session.visitorId);
                    client.Close();
                    GetAllUsers();
                }
                catch(Exception exc)
                {
                    MessageBox.Show(exc.Message, "Sikertelen végrehajtás!"); 
                }
            }
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            UserRecordHandler window = new UserRecordHandler(null);
            window.ShowDialog();
            if (window.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    UserServiceClient client = new UserServiceClient();
                    client.AddUser(window.User, Session.visitorId);
                    client.Close();
                    GetAllUsers();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
                }
            }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            GetAllUsers();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            User user = GetSelectedUserFromGrid();
            if (user == null) return;

            try
            {
                UserServiceClient client = new UserServiceClient();
                client.DeleteUser(user.id, Session.visitorId);
                client.Close();
                GetAllUsers();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }

        private void deleteAllButton_Click(object sender, EventArgs e)
        {
            try
            {
                UserServiceClient client = new UserServiceClient();
                client.DeleteAllUser(Session.visitorId);
                client.Close();
                GetAllUsers();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }

        private void filterButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.Text = simpleWindowText + " (Szűrt)";
                string filterString = FilterStringBuilder.Replace(filterTextBox.Text);
                UserServiceClient client = new UserServiceClient();
                User[] users = client.GetUsersFiltered(filterString, Session.visitorId);
                client.Close();
                RefreshGridView(users);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Sikertelen végrehajtás!");
            }
        }
    }
}
