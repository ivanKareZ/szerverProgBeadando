﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BandClient
{
    static class FilterStringBuilder
    {
        public static string Replace(string filterText)
        {
            return filterText.Replace('*', '%').Replace('?','_');
        }
    }
}
