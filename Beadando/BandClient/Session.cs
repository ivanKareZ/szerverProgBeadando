﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BandClient
{
    public static class Session
    {
        public static int visitorId { get; set; }
        public static string userName { get; set; }
        public static DateTime sessionStartTime { get; set; }

        static Session()
        {
            visitorId = -1;
            userName = "Ismeretlen";
        }

        public static void StartSession(int pvisitorId, string puserName)
        {
            visitorId = pvisitorId;
            userName = puserName;
            sessionStartTime = DateTime.Now;
        }
    }
}
