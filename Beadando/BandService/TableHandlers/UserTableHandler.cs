﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BandService.Models;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;

namespace BandService.TableHandlers
{
    public class UserTableHandler : TableHandler
    {
        public UserTableHandler() : base() { }

        internal List<User> GetAllUsers()
        {
            string query = "SELECT * FROM [User]";
            SqlCommand command = new SqlCommand(query);
            command.CommandType = CommandType.Text;
            DataTable result = dataBase.ExecuteCommand(command);
            return GetUserFromResult(result);
        }

        internal List<User> GetUsersFiltered(string name)
        {
            string procName = "FilterUser";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("filter", name));
            DataTable result = dataBase.ExecuteCommand(command);
            return GetUserFromResult(result);
        }

        internal void AddUser(User user)
        {
            string procName = "AddUser";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("fullname", user.fullName));
            command.Parameters.Add(new SqlParameter("username", user.userName));
            command.Parameters.Add(new SqlParameter("password", user.passWord));
            command.Parameters.Add(new SqlParameter("isadmin", user.isAdmin));
            dataBase.ExecuteNonQuery(command);

        }

        internal void UpdateUser(User user)
        {
            string procName = "UpdateUser";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("id", user.id));
            command.Parameters.Add(new SqlParameter("fullname", user.fullName));
            command.Parameters.Add(new SqlParameter("username", user.userName));
            command.Parameters.Add(new SqlParameter("password", user.passWord));
            command.Parameters.Add(new SqlParameter("isadmin", user.isAdmin));
            dataBase.ExecuteNonQuery(command);
        }

        internal void DeleteUser(int id)
        {
            string procName = "DeleteUser";
            SqlCommand command = new SqlCommand(procName);
            command.Parameters.Add(new SqlParameter("id", id));
            command.CommandType = CommandType.StoredProcedure;
            dataBase.ExecuteNonQuery(command);
        }

        internal void DeleteAllUser()
        {
            string procName = "DeleteAllUser";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            dataBase.ExecuteNonQuery(command);
        }

        List<User> GetUserFromResult(DataTable result)
        {
            List<User> users = new List<User>();
            for (int i = 0; i < result.Rows.Count; i++)
            {
                try {
                    int id = int.Parse(result.Rows[i]["id"].ToString());
                    string fullname = result.Rows[i]["fullname"].ToString();
                    string username = result.Rows[i]["username"].ToString();
                    string password = result.Rows[i]["password"].ToString();
                    bool isAdmin = bool.Parse(result.Rows[i]["isadmin"].ToString());
                    DateTime regDate = DateTime.Parse(result.Rows[i]["regdate"].ToString());

                    users.Add(new User(id, fullname, username, password, isAdmin, regDate)); }
                catch { }
            }  
            return users;
        }
    }
}