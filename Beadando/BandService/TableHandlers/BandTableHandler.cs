﻿using BandService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace BandService.TableHandlers
{
    public class BandTableHandler : TableHandler
    {
        public BandTableHandler() : base() { }

        internal List<Band> GetAllBand()
        {
            string procName = "AllBand";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            DataTable result = dataBase.ExecuteCommand(command);
            return GetBandFromResult(result);
        }

        internal List<Band> GetBandsFiltered(string name)
        {
            string procName = "FilterBand";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("filter", name));
            DataTable result = dataBase.ExecuteCommand(command);
            return GetBandFromResult(result);
        }

        internal void AddBand(Band band)
        {
            string procName = "AddBand";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("name", band.bandName));
            command.Parameters.Add(new SqlParameter("formation", band.formationDate));
            command.Parameters.Add(new SqlParameter("description", band.description));
            int id = new GenreTableHandler().GetGenreIdByName(band.genre);
            command.Parameters.Add(new SqlParameter("genreid", id));
            dataBase.ExecuteNonQuery(command);
        }

        internal void UpdateBand(Band band)
        {
            string procName = "UpdateBand";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("id", band.id));
            command.Parameters.Add(new SqlParameter("name", band.bandName));
            command.Parameters.Add(new SqlParameter("formation", band.formationDate));
            command.Parameters.Add(new SqlParameter("description", band.description));
            int id = new GenreTableHandler().GetGenreIdByName(band.genre);
            command.Parameters.Add(new SqlParameter("genreid", id));
            dataBase.ExecuteNonQuery(command);
        }

        internal void DeleteBand(int id)
        {
            string procName = "DeleteBand";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("id", id));
            dataBase.ExecuteNonQuery(command);
        }

        internal void DeleteAllBand()
        {
            string procName = "DeleteAllBand";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            dataBase.ExecuteNonQuery(command);
        }

        private List<Band> GetBandFromResult(DataTable result)
        {
            List<Band> bands = new List<Band>();
            for (int i = 0; i < result.Rows.Count; i++)
            {
                try
                {
                    int id = int.Parse(result.Rows[i]["id"].ToString());
                    string bandName = result.Rows[i]["name"].ToString();
                    string description = result.Rows[i]["description"].ToString();
                    string genre = result.Rows[i]["genre"].ToString();
                    DateTime formationDate = DateTime.Parse(result.Rows[i]["formation"].ToString());

                    bands.Add(new Band(id, bandName, formationDate, description, genre));
                }
                catch { }
            }
            return bands;
        }
    }
}