﻿using BandService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace BandService.TableHandlers
{
    public class BandMemberTableHandler : TableHandler
    {
        public BandMemberTableHandler() : base() { }

        public List<BandMembers> GetAll()
        {
            string procName = "AllBandMembers";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            DataTable result = dataBase.ExecuteCommand(command);
            return GetFromResult(result);
        }

        public List<BandMembers> GetAllFiltered(int bandId)
        {
            string procName = "FilteredBandMembers";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("bandid", bandId));
            DataTable result = dataBase.ExecuteCommand(command);
            return GetFromResult(result);
        }

        public List<User> GetUsersForBand(int bandId)
        {
            string procName = "GetUsersFromBand";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("bandid", bandId));
            DataTable result = dataBase.ExecuteCommand(command);
            return GetUsersFromResult(result);
        }

        public void Insert(BandMembers item)
        {
            string procName = "InsertBandMember";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("bandid", item.bandId));
            command.Parameters.Add(new SqlParameter("userid", item.userId));
            dataBase.ExecuteNonQuery(command);
        }

        public void Update(BandMembers item)
        {
            string procName = "UpdateBandMember";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("id", item.id));
            command.Parameters.Add(new SqlParameter("bandid", item.bandId));
            command.Parameters.Add(new SqlParameter("userid", item.userId));
            dataBase.ExecuteNonQuery(command);
        }

        public void Delete(int bid, int uid)
        {
            Debug.WriteLine("BID:" + bid + " UID:" + uid);
            string procName = "DeleteBandMember";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("bid", bid));
            command.Parameters.Add(new SqlParameter("uid", uid));
            dataBase.ExecuteNonQuery(command);
        }

        public void DeleteAll(int bid)
        {
            string procName = "DeleteAllBandMember";
            SqlCommand command = new SqlCommand(procName);
            command.Parameters.Add(new SqlParameter("bid", bid));
            command.CommandType = CommandType.StoredProcedure;
            dataBase.ExecuteNonQuery(command);
        }

        private List<BandMembers> GetFromResult(DataTable result)
        {
            List<BandMembers> items = new List<BandMembers>();
            for (int i = 0; i < result.Rows.Count; i++)
            {
                try
                {
                    int id = int.Parse(result.Rows[i]["id"].ToString());
                    int uid = int.Parse(result.Rows[i]["userid"].ToString());
                    int bid = int.Parse(result.Rows[i]["bandid"].ToString());

                    items.Add(new BandMembers(id, uid, bid));
                }
                catch { }
            }
            return items;
        }

        private List<User> GetUsersFromResult(DataTable result)
        {
            List<User> users = new List<User>();
            for (int i = 0; i < result.Rows.Count; i++)
            {
                try
                {
                    int id = int.Parse(result.Rows[i]["id"].ToString());
                    string fullname = result.Rows[i]["fullname"].ToString();
                    string username = result.Rows[i]["username"].ToString();
                    string password = result.Rows[i]["password"].ToString();
                    bool isAdmin = bool.Parse(result.Rows[i]["isadmin"].ToString());
                    DateTime regDate = DateTime.Parse(result.Rows[i]["regdate"].ToString());

                    users.Add(new User(id, fullname, username, password, isAdmin, regDate));
                }
                catch { }
            }
            return users;
        }
    }
}