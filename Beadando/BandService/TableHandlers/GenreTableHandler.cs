﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using BandService.Models;

namespace BandService.TableHandlers
{
    public class GenreTableHandler : TableHandler
    {
        public GenreTableHandler() : base() { }

        public int GetGenreIdByName(string name)
        {
            string query = "SELECT id FROM [Genre] WHERE name ='"+name+"'";
            SqlCommand command = new SqlCommand(query);
            command.CommandType = CommandType.Text;
            DataTable result = dataBase.ExecuteCommand(command);
            if (result.Rows.Count == 0)
                return -1;
            return int.Parse(result.Rows[0]["id"].ToString());
        }

        internal List<Genre> GetAllGenres()
        {
            string query = "SELECT * FROM [Genre]";
            SqlCommand command = new SqlCommand(query);
            command.CommandType = CommandType.Text;
            DataTable result = dataBase.ExecuteCommand(command);
            return GetGenreFromResult(result);
        }

        internal void DeleteAllGenres()
        {
            string procName = "DeleteAllGenre";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            dataBase.ExecuteNonQuery(command);
        }

        internal void AddGenre(Genre genre)
        {
            string procName = "AddGenre";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("name", genre.name));
            command.Parameters.Add(new SqlParameter("description", genre.description));
            dataBase.ExecuteNonQuery(command);
        }

        internal void DeleteGenre(int id)
        {
            string procName = "DeleteGenre";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("id", id));
            dataBase.ExecuteNonQuery(command);
        }

        internal List<Genre> GetGenresFiltered(string filter)
        {
            string query = "SELECT * FROM [Genre] WHERE name LIKE '"+filter+"'";
            SqlCommand command = new SqlCommand(query);
            command.CommandType = CommandType.Text;
            DataTable result = dataBase.ExecuteCommand(command);
            return GetGenreFromResult(result);
        }

        internal void UpdateGenre(Genre genre)
        {
            string procName = "UpdateGenre";
            SqlCommand command = new SqlCommand(procName);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("id", genre.id));
            command.Parameters.Add(new SqlParameter("name", genre.name));
            command.Parameters.Add(new SqlParameter("description", genre.description));
            dataBase.ExecuteNonQuery(command);
        }

        private List<Genre> GetGenreFromResult(DataTable result)
        {
            List<Genre> bands = new List<Genre>();
            for (int i = 0; i < result.Rows.Count; i++)
            {
                try
                {
                    int id = int.Parse(result.Rows[i]["id"].ToString());
                    string name = result.Rows[i]["name"].ToString();
                    string description = result.Rows[i]["description"].ToString();

                    bands.Add(new Genre(id, name, description));
                }
                catch { }
            }
            return bands;
        }
    }
}