﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BandService.Database;

namespace BandService.TableHandlers
{
    public abstract class TableHandler
    {
        protected DataBase dataBase;

        public TableHandler()
        {
            dataBase = DataBase.GetInstance();
        }
    }
}