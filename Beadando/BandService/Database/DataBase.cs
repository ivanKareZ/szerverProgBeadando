﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;

namespace BandService.Database
{
    public class DataBase
    {
        static DataBase instance = new DataBase();
        SqlConnection sqlconnection;

        protected DataBase()
        {
            string connectionString = "Data Source=localhost\\SQLEXPRESS;Initial Catalog=banddatabase;Integrated Security=True";
            this.sqlconnection = new SqlConnection(connectionString);
        }

        public DataTable ExecuteCommand(SqlCommand command)
        {
            command.Connection = sqlconnection;
            DataTable dt = new DataTable();
            try
            {
                if (sqlconnection.State != ConnectionState.Open) sqlconnection.Open();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                sda.Fill(dt);
            }
            catch(Exception ex)
            {
                Debug.WriteLine("COMMAND FAULT: "+ex.Message);
            }
            finally
            {
                sqlconnection.Close();
            }
            return dt;
        }

        public void ExecuteNonQuery(SqlCommand command)
        {
            command.Connection = sqlconnection;
            Exception exc = null;
            try
            {
                if (sqlconnection.State != ConnectionState.Open) sqlconnection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("COMMAND FAULT: " + ex.Message);
                if (ex.Message.ToLower().Contains("conflicted with the reference constraint"))
                    exc = new Exception("A kért műveletet nem lehet végrehajtani, mert egy másik táblából hivatkzonak a rekordra! A törléshez előbb meg kell szüntetni a hivatkozást.");
                else
                    exc = ex;
            }
            finally
            {
                sqlconnection.Close();
            }
            if (exc != null) throw exc;
        }

        public static DataBase GetInstance()
        {
            return instance;
        }
    }
}