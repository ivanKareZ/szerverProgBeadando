﻿using BandService.TableHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BandService.ServiceWork;
using BandService.Models;

namespace BandService.Services
{
    public class UserService : IUserService
    {
        UserTableHandler userTableHandler;

        public UserService()
        {
            this.userTableHandler = new UserTableHandler();
        }

        public List<User> GetAllUser(int visitorId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                return userTableHandler.GetAllUsers();
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public int LoginUser(string username, string password)
        {
            if (username == "Admin" && password == "Admin")
                return VisitorIdManager.GetInstance().GenerateNewId();
            throw new FaultException("A felhasználónév vagy jelszó nem helyes!");
        }

        public void AddUser(User user, int visitorId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                userTableHandler.AddUser(user);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void UpdateUser(User user, int visitorId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                userTableHandler.UpdateUser(user);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void DeleteUser(int id, int visitorId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                userTableHandler.DeleteUser(id);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void DeleteAllUser(int visitorId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                userTableHandler.DeleteAllUser();
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public List<User> GetUsersFiltered(string name, int visitorId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                return userTableHandler.GetUsersFiltered(name);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }
    }
}
