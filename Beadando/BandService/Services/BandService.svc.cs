﻿using BandService.ServiceWork;
using BandService.TableHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BandService.Services
{
    public class BandService : IBandService
    {
        BandTableHandler tableHandler;

        public BandService()
        {
            tableHandler = new BandTableHandler();
        }

        public List<Models.Band> GetAllBand(int visitorId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                return tableHandler.GetAllBand();
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public List<Models.Band> GetBandsFiltered(int visitorId, string filter)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                return tableHandler.GetBandsFiltered(filter);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void UpdateBand(int visitorId, Models.Band band)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                tableHandler.UpdateBand(band);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void AddBand(int visitorId, Models.Band band)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                tableHandler.AddBand(band);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void DeleteBand(int visitorId, int id)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                tableHandler.DeleteBand(id);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void DeleteAllBands(int visitorId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                tableHandler.DeleteAllBand();
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }
    }
}
