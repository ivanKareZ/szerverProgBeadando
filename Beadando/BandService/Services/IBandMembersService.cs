﻿using BandService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BandService.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBandMembersService" in both code and config file together.
    [ServiceContract]
    public interface IBandMembersService
    {
        [OperationContract]
        [FaultContract(typeof(FaultException))]
        List<BandMembers> GetAll(int visitorId);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        List<BandMembers> GetAllFiltered(int visitorId,int bandId);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void Insert(int visitorId,BandMembers item);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void Update(int visitorId,BandMembers item);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void Delete(int visitorId,int bid, int uid);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void DeleteAll(int visitorId, int bid);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        List<User> GetUsersFromBand(int visitorId, int bandId);
    }
}
