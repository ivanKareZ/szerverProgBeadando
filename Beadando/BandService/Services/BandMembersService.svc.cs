﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BandService.Models;
using BandService.ServiceWork;
using BandService.TableHandlers;

namespace BandService.Services
{
    public class BandMembersService : IBandMembersService
    {
        BandMemberTableHandler tableHandler;

        public BandMembersService()
        {
            tableHandler = new BandMemberTableHandler();
        }

        public void Delete(int visitorId, int uid, int bid)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                tableHandler.Delete(bid, uid);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void DeleteAll(int visitorId, int bid)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                tableHandler.DeleteAll(bid);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public List<BandMembers> GetAll(int visitorId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                return tableHandler.GetAll();
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public List<BandMembers> GetAllFiltered(int visitorId, int bandId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                return tableHandler.GetAllFiltered(bandId);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public List<User> GetUsersFromBand(int visitorId, int bandId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                return tableHandler.GetUsersForBand(bandId);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void Insert(int visitorId, BandMembers item)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                tableHandler.Insert(item);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void Update(int visitorId, BandMembers item)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                tableHandler.Update(item);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }
    }
}
