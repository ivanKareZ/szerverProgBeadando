﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BandService.Models;
using BandService.TableHandlers;
using BandService.ServiceWork;

namespace BandService.Services
{
    public class GenreService : IGenreService
    {
        GenreTableHandler tableHandler;

        public GenreService()
        {
            tableHandler = new GenreTableHandler();
        }

        public List<Genre> GetAllGenres(int visitorId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                return tableHandler.GetAllGenres();
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void AddGenre(int visitorId, Genre genre)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                tableHandler.AddGenre(genre);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void DeleteAllGenres(int visitorId)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                tableHandler.DeleteAllGenres();
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void DeleteGenre(int visitorId, int id)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                tableHandler.DeleteGenre(id);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public List<Genre> GetGenresFiltered(int visitorId, string filter)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                return tableHandler.GetGenresFiltered(filter);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void UpdateGenre(int visitorId, Genre genre)
        {
            if (!VisitorIdManager.GetInstance().IsLoggedIn(visitorId)) throw new FaultException("A művelet végrehajtásához be kell jelentkezned!");
            try
            {
                tableHandler.UpdateGenre(genre);
            }
            catch (Exception e)
            {
                throw new FaultException(e.Message);
            }
        }
    }
}
