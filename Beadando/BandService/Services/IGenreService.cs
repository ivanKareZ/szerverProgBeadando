﻿using BandService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BandService.Services
{
    [ServiceContract]
    public interface IGenreService
    {
        [OperationContract]
        [FaultContract(typeof(FaultException))]
        List<Genre> GetAllGenres(int visitorId);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        List<Genre> GetGenresFiltered(int visitorId, string filter);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void UpdateGenre(int visitorId, Genre genre);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void AddGenre(int visitorId, Genre genre);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void DeleteGenre(int visitorId, int id);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void DeleteAllGenres(int visitorId);
    }
}
