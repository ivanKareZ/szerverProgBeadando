﻿using BandService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BandService.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBandService" in both code and config file together.
    [ServiceContract]
    public interface IBandService
    {
        [OperationContract]
        [FaultContract(typeof(FaultException))]
        List<Band> GetAllBand(int visitorId);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        List<Band> GetBandsFiltered(int visitorId, string filter);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void UpdateBand(int visitorId, Band band);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void AddBand(int visitorId, Band band);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void DeleteBand(int visitorId, int id);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void DeleteAllBands(int visitorId);
    }
}
