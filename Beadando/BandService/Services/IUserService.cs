﻿using BandService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BandService.Services
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        [FaultContract(typeof(FaultException))]
        List<User> GetAllUser(int visitorId);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        int LoginUser(string username, string password);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void AddUser(User user, int visitorId);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void UpdateUser(User user, int visitorId);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void DeleteUser(int id, int visitorId);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        void DeleteAllUser(int visitorId);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        List<User> GetUsersFiltered(string name, int visitorId);
    }
}
