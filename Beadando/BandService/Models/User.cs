﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BandService.Models
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string fullName { get; set; }
        [DataMember]
        public string userName { get; set; }
        [DataMember]
        public string passWord { get; set; }
        [DataMember]
        public bool isAdmin { get; set; }
        [DataMember]
        public DateTime regDate { get; set; }

        public User(int id, string fullName, string userName, string passWord, bool isAdmin, DateTime regDate)
        {
            this.id = id;
            this.fullName = fullName;
            this.userName = userName;
            this.passWord = passWord;
            this.isAdmin = isAdmin;
            this.regDate = regDate;
        }
        
    }
}