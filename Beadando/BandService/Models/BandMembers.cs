﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BandService.Models
{
    [DataContract]
    public class BandMembers
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public int userId { get; set; }
        [DataMember]
        public int bandId { get; set; }

        public BandMembers(int id, int userId, int bandId)
        {
            this.id = id;
            this.userId = userId;
            this.bandId = bandId;
        }
    }
}