﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BandService.Models
{
    [DataContract]
    public class Band
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string bandName { get; set; }
        [DataMember]
        public DateTime formationDate { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string genre { get; set; }

        public Band(int id, string bandName, DateTime formationDate, string description, string genre)
        {
            this.id = id;
            this.bandName = bandName;
            this.formationDate = formationDate;
            this.description = description;
            this.genre = genre;
        }
    }
}