﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandService.ServiceWork
{
    public class VisitorIdManager
    {
        static VisitorIdManager instance = new VisitorIdManager();
        List<int> visitorIds;
        int currentId;

        protected VisitorIdManager()
        {
            currentId = 0;
            visitorIds = new List<int>();
        }

        public int GenerateNewId()
        {
            currentId++;
            visitorIds.Add(currentId);
            return currentId;
        }

        public bool IsLoggedIn(int visitorId)
        {
            return visitorIds.Contains(visitorId);
        }

        public void RemoveId(int visitorId)
        {
            visitorIds.Remove(visitorId);
        }

        public static VisitorIdManager GetInstance()
        {
            return instance;
        }
    }
}